import { Component, OnInit } from '@angular/core';

//
import	{ActivatedRoute,	Router}	from	'@angular/router';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {

  public maVariable	:	string;
  constructor(public	activatedRoute:	ActivatedRoute) {
    this.maVariable	=	this.activatedRoute.snapshot.paramMap.get('maVariable');
  }

/*
public showPic(){
  let data : Observable<any>;
  data = this.http.get('https://jsonplaceholder.typicode.com/photos?albumId=1');
  data.subscribe(result=>{this.monTableau = result;})
}
*/

  ngOnInit() {
  }

}
