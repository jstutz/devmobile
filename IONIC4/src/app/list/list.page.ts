import { Component, OnInit } from '@angular/core';

//mes imports:
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute,	Router}	from	'@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})

export class ListPage implements OnInit {
  /*
  private selectedItem: any;
  private icons = [
    'flask',
    'wifi',
    'beer',
    'football',
    'basketball',
    'paper-plane',
    'american-football',
    'boat',
    'bluetooth',
    'build'
  ];
  */

  public monTableau: any;
  public items: Array<{ title: string; note: string; icon: string }> = [];

  constructor(public http : HttpClient, public router:Router) {
    this.liste();
  }

  public liste(){
    let data : Observable<any>;
    data = this.http.get('https://jsonplaceholder.typicode.com/photos?albumId=1');
    data.subscribe(result=>{this.monTableau = result;})
  }

  public detail(x){
    this.router.navigate(['detail',	x]);
  }


  ngOnInit() {
  }
}
